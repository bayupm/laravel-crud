@extends('backend.konten')


@section('judul','Pertanyaan')

@section('konten')

<a href="/pertanyaan/create" class="btn btn-success btn-sm mb-2">Tambah Pertanyaan</a>
<table class="table">
    <thead class="thead-dark">
      <tr class="text-center">
        <th scope="col">#</th>
        <th scope="col">Judul</th>
        <th scope="col">Isi Pertanyaan</th>
        <th scope="col">Aksi</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($pertanyaan as $per => $hasil)
      <tr class="text-center">
        
        <th scope="row">{{$loop->iteration}}</th>
        <td>{{$hasil->judul}}</td>
        <td>{{$hasil->isi_pertanyaan}}</td>
        <td>

            <form action="/pertanyaan/{{$hasil->id}}" method="POST">
                @csrf
                @method('delete')
                <a href="/pertanyaan/show/{{$hasil->id}}" class="btn btn-primary btn-sm">Lihat</a>
                <a href="/pertanyaan/{{$hasil->id}}/edit" class="btn btn-info btn-sm">Ubah</a>
                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
            </form>
        </td>
        @endforeach
        
      </tr>
    </tbody>
  </table>
@endsection