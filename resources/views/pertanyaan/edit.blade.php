@extends('backend.konten')


@section('judul','Edit Pertanyaan')

@section('konten')

<form action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label for="exampleInputEmail1">Judul</label>
      <input type="text" class="form-control" value="{{$pertanyaan->judul}}" name="judul" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Judul">
      
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Isi Pertanyaan</label>
      <input type="text" class="form-control" value="{{$pertanyaan->isi_pertanyaan}}" name="isi_pertanyaan">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection