<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $table = 'pertanyaan1';
    protected $fillable = ['judul', 'isi_pertanyaan'];
}
