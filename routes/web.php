<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/pertanyaan', 'PertanyaanController@index');
Route::get('/pertanyaan/{id}/edit', 'PertanyaanController@edit');
Route::get('/pertanyaan/show/{id}', 'PertanyaanController@show');
Route::put('/pertanyaan/{id}', 'PertanyaanController@update');
Route::delete('/pertanyaan/{id}','PertanyaanController@destroy');

Route::post('/pertanyaan' ,'PertanyaanController@store');
